FROM busybox:latest

RUN mkdir test &&\
    cd test &&\
    touch test.txt &&\
    echo "This is test" >> test.txt